import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';


import firebase from 'firebase';

@Component({
  selector: 'page-newAccount',
  templateUrl: 'newAccount.html'
})
export class NewAccount {

  email: string;
  password: string;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController) {
  }

  writeUserData(uId, email) {
    firebase.database().ref('users/' + uId).push({
      email: email
    });
  }

  onClickSendBtn(email, password) {
    if(email !== undefined && password !== undefined){
      firebase.auth().createUserWithEmailAndPassword(email, password)
      .then((result) => {
        this.writeUserData(result.uid, email);
        this.showAlert('Congratulations',"You account was created.");
        this.navCtrl.pop();
      })
      .catch((error) => {
        this.showAlert('Warning', error.message);
      });
    }else{
      this.showAlert('Warning', "There are fields that have not been filled.");
    }
  }

  showAlert(title, message){
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }
}
