import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import firebase from 'firebase';

@Component({
  selector: 'page-my-films',
  templateUrl: 'myFilms.html'
})
export class MyFilms {

  private database: any;
  private movies: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
    this.movies = this.getMyFilms();
  }

  getMyFilms(){
    var userId = firebase.auth().currentUser.uid;
    var movies = new Array();
    firebase.database().ref('/movies/' + userId).once('value').then(function(snapshot) {
      snapshot.forEach(function(childSnapshot) {
        var movie = childSnapshot.val().movie;
        movies.push(movie);
      });
    });
    return movies;
  }
}
