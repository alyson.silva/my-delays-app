import { Component, OnDestroy } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';

import firebase from 'firebase';

/**
 * Generated class for the FilmDetail page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-film-detail',
  templateUrl: 'filmDetail.html',
})

export class FilmDetail{

  movie: any;
  showCard: any;
  storage: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public alertCtrl: AlertController) {
    this.movie = navParams.get('movie');
    console.log(this.movie);
    this.showCard = true;
    this.storage = firebase.storage();
  }

  onFabClicked(movie){

    let uId = firebase.auth().currentUser.uid;
    firebase.database().ref('movies/' + uId).push({movie}).then((result) => {

      let alert = this.alertCtrl.create({
        title: "Alert",
        subTitle: "The movie was saved.",
        buttons: ['OK']
      });
      alert.present();
    });
  }

}
