import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import { FilmDetail } from '../filmDetail/filmDetail';


@Component({
  selector: 'page-film-list',
  templateUrl: 'filmList.html'
})

export class FilmList implements OnInit{
  search: any;
  movies: Array<any>;

  constructor(public navCtrl: NavController, public http: Http) {
  }

  ngOnInit(){
    if(this.search == undefined){
      this.search = '';
    }
  }

  onInput($event){
      if(this.search !== undefined){

        let test = this.search.split(' ').join('%');
        console.log(test);
        this.http.get("http://netflixroulette.net/api/api.php?title=" + this.search)
        .subscribe(res => {

          let response = res.json();
          this.movies = new Array();
    //      console.log(this.movies);
          console.log(res);
          console.log("--------------");
          console.log(response);
          this.movies.push(response);

        }, (err) => {
          console.log(err);
        });
      }
    }

  onMovieClicked(movie){
   this.navCtrl.push(FilmDetail,{movie:movie});
  }

}
