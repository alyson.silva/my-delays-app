import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { FilmList } from '../filmList/filmList';
import { NewAccount } from '../newAccount/newAccount';

import firebase from 'firebase';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class Login {
  newAccount = NewAccount;
  filmList = FilmList;

  email: string;
  password: string;
  loader: any;

  constructor(public navCtrl: NavController, private alertCtrl: AlertController, public loadingCtrl: LoadingController) {
  }


  onSignInClick(email, password){
    if(email !== undefined && password !== undefined){
      this.presentLoading();
      firebase.auth().signInWithEmailAndPassword(email, password)
      .then((result) => {
        this.dismissLoading();
        this.showAlert("Welcome","Congratulations, you signed in.");
        this.navCtrl.setRoot(FilmList);
      }).catch((error) => {
        this.dismissLoading();
        this.showAlert("Warning", error.message);
      });

    }else {
      this.showAlert('Warning', "There are fields that have not been filled.");
    }
  }

  onNoHaveAccount(){
    this.navCtrl.push(this.newAccount);
  }

  showAlert(title, message){
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    this.loader.present();
  }

  dismissLoading(){
    this.loader.dismiss();
  }

}
