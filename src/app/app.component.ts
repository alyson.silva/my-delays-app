import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Login } from '../pages/login/login';
import { NewAccount } from '../pages/newAccount/newAccount';
import { FilmList } from '../pages/filmList/filmList';
import { MyFilms } from '../pages/myFilms/myFilms';

import firebase from 'firebase';
import { UserModel } from '../model/userModel/userModel';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = Login;
  pages: Array<{title: string, component: any}>

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public alertCtrl: AlertController) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.pages = [
      { title: 'Search Movies', component: FilmList },
      { title: 'My Movies', component: MyFilms },
    ];
  }

  onLogoutClick(){
    this.showConfirm();
  }

  showConfirm() {
    let confirm = this.alertCtrl.create({
      title: 'Warning',
      message: 'Do you really want to leave?',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            console.log('Disagree Clicked');
          }
        },
        {
          text: 'Agree',
          handler: () => {
            firebase.auth().signOut().then((result) => {this.nav.setRoot(Login)});
          }
        }
      ]
    });
    confirm.present();
  }

  openPage(page) {
    this.nav.setRoot(page);
  }

}
