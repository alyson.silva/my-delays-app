import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';

import firebase from 'firebase';
import { AngularFireModule } from 'angularfire2';

import { Login } from '../pages/login/login';
import { FilmDetail } from '../pages/filmDetail/filmDetail';
import { NewAccount } from '../pages/newAccount/newAccount';
import { FilmList } from '../pages/filmList/filmList';
import { MyFilms } from '../pages/myFilms/myFilms';

export const firebaseConfig = {
  apiKey: "AIzaSyAaUFDw9uUHkx8PV9cddxbB5SBW2ONQDLg",
  authDomain: "mydelays-47b36.firebaseapp.com",
  databaseURL: "https://mydelays-47b36.firebaseio.com",
  projectId: "mydelays-47b36",
  storageBucket: "mydelays-47b36.appspot.com",
  messagingSenderId: "153056973888"
};

@NgModule({
  declarations: [
    MyApp,
    Login,
    NewAccount,
    FilmDetail,
    FilmList,
    MyFilms

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp,
   {
      platforms : {
        android : {
          scrollAssist: false,    // Valid options appear to be [true, false]
          autoFocusAssist: false  // Valid options appear to be ['instant', 'delay', false]
        }
      }
    }
  ),
    AngularFireModule.initializeApp(firebaseConfig),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Login,
    FilmDetail,
    NewAccount,
    FilmList,
    MyFilms
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {

  constructor(){
    firebase.initializeApp(firebaseConfig);
  }

}
